<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Biolife - Organic Food</title>
        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/slick.min.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/main-color.css">

    </head>

    <body class="biolife-body">

        <jsp:include page="./layout/header.jsp"/>


        <!--Navigation  section-->
        <div class="container">
            <nav class="biolife-nav">
                <ul>
                    <li class="nav-item"><a href="index-2.html" class="permal-link">Home</a></li>
                    <li class="nav-item"><a href="productdetails.html" class="permal-link">Product List</a></li>
                </ul>
            </nav>
        </div>
        <div class="page-contain category-page no-sidebar">
            <div class="container">
                <div class="row">
                    <!-- Main content -->
                    <div id="main-content" class="main-content col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="product-category grid-style">

                            <div id="top-functions-area" class="top-functions-area">
                                <form action="#" name="frm-refine" method="get">

                                    <div class="flt-item to-left group-on-mobile">
                                        <span class="flt-title">Filter</span>
                                        <a href="productdetails.html" class="icon-for-mobile">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </a>
                                        <div class="wrap-selectors">
                                            <span class="title-for-mobile">Filter Products By</span>
                                            <div data-title="Price:" class="selector-item">
                                                <select name="price" class="selector">
                                                    <option value="all">Price</option>
                                                    <option value="class-1st">Less than 5$</option>
                                                    <option value="class-2nd">$5-10$</option>
                                                    <option value="class-3rd">$10-20$</option>
                                                    <option value="class-4th">$20-45$</option>
                                                    <option value="class-5th">$45-100$</option>
                                                    <option value="class-6th">$100-150$</option>
                                                    <option value="class-7th">More than 150$</option>
                                                </select>
                                            </div>
                                            <div data-title="Brand:" class="selector-item">
                                                <select name="brad" class="selector">
                                                    <option value="all">Brands</option>
                                                    <option value="br2">Brand first</option>
                                                    <option value="br3">Brand second</option>
                                                    <option value="br4">Brand third</option>
                                                    <option value="br5">Brand fourth</option>
                                                    <option value="br6">Brand fiveth</option>
                                                </select>
                                            </div>
                                            <p class="btn-for-mobile"><button type="submit" class="btn-submit">Go</button></p>
                                        </div>
                                    </div>
                                    <div class="flt-item to-right">
                                        <span class="flt-title">Sort</span>
                                        <div class="wrap-selectors">
                                            <div class="selector-item orderby-selector">
                                                <select name="orderby" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>
                                                    <option value="popularity">best sell</option>
                                                    <option value="date">newness</option>
                                                    <option value="price">price: low to high</option>
                                                    <option value="price-desc">price: high to low</option>
                                                </select>
                                            </div>
                                            <div class="selector-item viewmode-selector">
                                                <a href="category-grid-left-sidebar.html" class="viewmode grid-mode active"><i class="biolife-icon icon-grid"></i></a>
                                                <a href="category-list-left-sidebar.html" class="viewmode detail-mode"><i class="biolife-icon icon-list"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 


                            <div class="row">
                                <ul class="products-list">

                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-11.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">
                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-13.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-21.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-14.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-15.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-16.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-17.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-18.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-10.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-11.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-09.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-08.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-10.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">
                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-17.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="product-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <div class="contain-product layout-default">
                                            <div class="product-thumb">
                                                <a href="productdetails.html" class="link-to-product">
                                                    <img src="assets/images/products/p-14.jpg" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">Fresh Fruit</b>
                                                <h4 class="product-title"><a href="productdetails.html" class="pr-name">National Fresh Fruit</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount"><span class="currencySymbol">�</span>85.00</span></ins>
                                                    <del><span class="price-amount"><span class="currencySymbol">�</span>95.00</span></del>
                                                </div>
                                                <div class="slide-down-box">

                                                    <div class="buttons">
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Buy</a>
                                                        <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <div class="biolife-panigations-block">
                                <ul class="panigation-contain">
                                    <li><a href="productdetails.html" class="link-page next"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                    <li><span class="link-page">1</span></li>
                                    <li><a href="productdetails.html" class="link-page">2</a></li>
                                    <li><a href="productdetails.html" class="current-page">3</a></li>
                                    <li><span class="sep">....</span></li>
                                    <li><a href="productdetails.html" class="link-page">20</a></li>
                                    <li><a href="productdetails.html" class="link-page next"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <jsp:include page="./layout/footer.jsp"/>
        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
    </body>

</html>