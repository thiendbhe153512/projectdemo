<%-- 
    Document   : home
    Created on : May 22, 2022, 2:59:33 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Biolife - Organic Food</title>
        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main-color.css">

    </head>

    <body class="biolife-body">
        <jsp:include page="./layout/header.jsp"/>

        <!-- Page Contain -->
        <div class="page-contain">

            <!-- Main content -->
            <div id="main-content" class="main-content">

                <!--Block 01: Main slide-->
                <div class="main-slide block-slider">
                    <ul class="biolife-carousel nav-none-on-mobile" data-slick='{"arrows": true, "dots": false, "slidesMargin": 0, "slidesToShow": 1, "infinite": true, "speed": 800}'>
                        <li>
                            <div class="slide-contain slider-opt03__layout01">
                                <div class="media">
                                    <div class="child-elememt">
                                        <a href="productdetails.html" class="link-to">
                                            <img src="assets/images/home-03/slide-01-child-01.png" width="604" height="580" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="text-content">
                                    <h3 class="second-line">Title</h3>

                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="slide-contain slider-opt03__layout01">
                                <div class="media">
                                    <div class="child-elememt">
                                        <a href="productdetails.html" class="link-to"><img src="assets/images/home-03/slide-01-child-01.png" width="604" height="580" alt=""></a>
                                    </div>
                                </div>
                                <div class="text-content">
                                    <h3 class="second-line">Vegetables 100% Organic</h3>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <!--Block 02: Banner-->
                <div class="wrap-category sm-margin-top-54px xs-margin-top-80px sm-margin-bottom-70px xs-margin-bottom-80px">
                    <div class="container">
                        <div class="biolife-title-box bold-style biolife-title-box__bold-style xs-margin-bottom-36px xs-margin-top-0">
                            <h3 style="display: flex;justify-content: center;" class="title">All Categories</h3>
                        </div>
                        <ul class="biolife-carousel nav-center-bold nav-none-on-mobile slick-initialized slick-slider" data-slick="{&quot;arrows&quot;:true,&quot;dots&quot;:false,&quot;infinite&quot;:false,&quot;speed&quot;:400,&quot;slidesMargin&quot;:30,&quot;slidesToShow&quot;:4, &quot;responsive&quot;:[{&quot;breakpoint&quot;:1200, &quot;settings&quot;:{ &quot;slidesToShow&quot;: 3}},{&quot;breakpoint&quot;:992, &quot;settings&quot;:{ &quot;slidesToShow&quot;: 3}},{&quot;breakpoint&quot;:768, &quot;settings&quot;:{ &quot;slidesToShow&quot;: 2}}, {&quot;breakpoint&quot;:500, &quot;settings&quot;:{ &quot;slidesToShow&quot;: 1}}]}"><span class="biolife-icon icon-left-arrow prev slick-arrow slick-disabled" aria-disabled="true"></span>
                            <div class="slick-list draggable">
                                <div class="slick-track" style="opacity: 1; width: 2580px; transform: translate3d(0px, 0px, 0px);">
                                    <li class="slick-slide slick-current slick-active first-slick" data-slick-index="0" aria-hidden="false" tabindex="0" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="0">
                                                    <img src="assets/images/home-04/cat-thumb01.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="0">
                                                <h4 class="cat-name">Fresh Fruit</h4>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="0" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="0">
                                                    <img src="assets/images/home-04/cat-thumb02.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="0">
                                                <h4 class="cat-name">Drink Fruits</h4>
                                                <span class="cat-number">(220 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="0" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="0">
                                                    <img src="assets/images/home-04/cat-thumb03.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="0">
                                                <h4 class="cat-name">vegetables</h4>
                                                <span class="cat-number">(350 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide slick-active last-slick" data-slick-index="3" aria-hidden="false" tabindex="0" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="0">
                                                    <img src="assets/images/home-04/cat-thumb04.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="0">
                                                <h4 class="cat-name">Dried Fruits</h4>
                                                <span class="cat-number">(520 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide" data-slick-index="4" aria-hidden="true" tabindex="-1" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="-1">
                                                    <img src="assets/images/home-04/cat-thumb01.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="-1">
                                                <h4 class="cat-name">Fresh Fruit</h4>
                                                <span class="cat-number">(20 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide" data-slick-index="5" aria-hidden="true" tabindex="-1" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="-1">
                                                    <img src="assets/images/home-04/cat-thumb02.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="-1">
                                                <h4 class="cat-name">Drink Fruits</h4>
                                                <span class="cat-number">(220 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="-1">
                                                    <img src="assets/images/home-04/cat-thumb03.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="-1">
                                                <h4 class="cat-name">vegetables</h4>
                                                <span class="cat-number">(350 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="slick-slide" data-slick-index="7" aria-hidden="true" tabindex="-1" style="margin-right: 30px; width: 270px;">
                                        <div class="biolife-cat-box-item">
                                            <div class="cat-thumb">
                                                <a href="#" class="cat-link" tabindex="-1">
                                                    <img src="assets/images/home-04/cat-thumb04.jpg" width="277" height="185" alt="">
                                                </a>
                                            </div>
                                            <a class="cat-info" href="#" tabindex="-1">
                                                <h4 class="cat-name">Dried Fruits</h4>
                                                <span class="cat-number">(520 items)</span>
                                            </a>
                                        </div>
                                    </li>
                                </div>
                            </div>

                            <span class="biolife-icon icon-arrow-right next slick-arrow" aria-disabled="false"></span></ul>
                    </div>
                </div>
                <!--Block 03: Product Tab-->
                <div class="product-tab z-index-20 sm-margin-top-193px xs-margin-top-30px">
                    <div class="container">

                        <div class="biolife-tab biolife-tab-contain sm-margin-top-34px">
                            <div class="tab-head tab-head__icon-top-layout icon-top-layout">
                                <ul class="tabs md-margin-bottom-35-im xs-margin-bottom-40-im">
                                    <li class="tab-element active">
                                        <a href="#tab01_1st" class="tab-link">HOT PRODUCT</a>
                                    </li>
                                    <li class="tab-element">
                                        <a href="#tab01_2nd" class="tab-link">FEATURE PRODUCT</a>
                                    </li>
                                    <li class="tab-element">
                                        <a href="#tab01_3rd" class="tab-link">ALL PRODUCT</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab-content">
                                <div id="tab01_1st" class="tab-contain active">
                                    <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile eq-height-contain" data-slick='{"rows":2 ,"arrows":true,"dots":false,"infinite":true,"speed":400,"slidesMargin":10,"slidesToShow":4, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 4}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":25 }},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":15}}]}'>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div id="tab01_2nd" class="tab-contain ">
                                    <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile eq-height-contain" data-slick='{"rows":2 ,"arrows":true,"dots":false,"infinite":true,"speed":400,"slidesMargin":10,"slidesToShow":4, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 4}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":25 }},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":15}}]}'>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                                <div id="tab01_3rd" class="tab-contain ">
                                    <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile eq-height-contain" data-slick='{"rows":2 ,"arrows":true,"dots":false,"infinite":true,"speed":400,"slidesMargin":10,"slidesToShow":4, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 4}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":25 }},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":15}}]}'>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li><li class="product-item">
                                            <div class="contain-product layout-default">
                                                <div class="product-thumb">
                                                    <a href="productdetails.html" class="link-to-product">
                                                        <img src="assets/images/products/p-05.jpg" alt="Vegetables" width="270" height="270" class="product-thumnail">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <b class="categories">Vegetables123</b>
                                                    <h4 class="product-title"><a href="productdetails.html" class="pr-name">Organic Hass Avocado, Large</a></h4>
                                                    <div class="price ">
                                                        <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                                                        <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                                                        <div class="buttons">
                                                            <a href="productdetails.html" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>






                <!--Block 08: Blog Posts-->
                <div class="blog-posts sm-margin-top-93px sm-padding-top-72px xs-padding-bottom-50px">
                    <div class="container">
                        <div class="biolife-title-box">
                            <h3 class="main-title">From the Blog</h3>
                        </div>
                        <ul class="biolife-carousel nav-center nav-none-on-mobile xs-margin-top-36px" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":30,"slidesToShow":3, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 3}},{"breakpoint":992, "settings":{ "slidesToShow": 2}},{"breakpoint":768, "settings":{ "slidesToShow": 2}},{"breakpoint":600, "settings":{ "slidesToShow": 1}}]}'>
                            <li>
                                <div class="post-item effect-01 style-bottom-info layout-02 ">
                                    <div class="thumbnail">
                                        <a href="blogdetails.html" class="link-to-post"><img src="assets/images/our-blog/post-thumb-01.jpg" width="370" height="270" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <h4 class="post-name"><a href="productdetails.html" class="linktopost">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                        <div class="post-meta">
                                            <a href="blogdetails.html" class="post-meta__item author"><img src="assets/images/home-03/post-author.png" width="28" height="28" alt=""><span>Admin</span></a>
                                        </div>
                                        <p class="excerpt">Did you know that red-staining foods are excellent lymph-movers? In fact, many plants that were historically used as dyes...</p>
                                        <div class="group-buttons">
                                            <a href="blogdetails.html" class="btn readmore">continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-item effect-01 style-bottom-info layout-02 ">
                                    <div class="thumbnail">
                                        <a href="blogdetails.html" class="link-to-post"><img src="assets/images/our-blog/post-thumb-01.jpg" width="370" height="270" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <h4 class="post-name"><a href="productdetails.html" class="linktopost">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                        <div class="post-meta">
                                            <a href="blogdetails.html" class="post-meta__item author"><img src="assets/images/home-03/post-author.png" width="28" height="28" alt=""><span>Admin</span></a>
                                        </div>
                                        <p class="excerpt">Did you know that red-staining foods are excellent lymph-movers? In fact, many plants that were historically used as dyes...</p>
                                        <div class="group-buttons">
                                            <a href="blogdetails.html" class="btn readmore">continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-item effect-01 style-bottom-info layout-02 ">
                                    <div class="thumbnail">
                                        <a href="blogdetails.html" class="link-to-post"><img src="assets/images/our-blog/post-thumb-01.jpg" width="370" height="270" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <h4 class="post-name"><a href="productdetails.html" class="linktopost">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                        <div class="post-meta">
                                            <a href="blogdetails.html" class="post-meta__item author"><img src="assets/images/home-03/post-author.png" width="28" height="28" alt=""><span>Admin</span></a>
                                        </div>
                                        <p class="excerpt">Did you know that red-staining foods are excellent lymph-movers? In fact, many plants that were historically used as dyes...</p>
                                        <div class="group-buttons">
                                            <a href="blogdetails.html" class="btn readmore">continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-item effect-01 style-bottom-info layout-02 ">
                                    <div class="thumbnail">
                                        <a href="blogdetails.html" class="link-to-post"><img src="assets/images/our-blog/post-thumb-01.jpg" width="370" height="270" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <h4 class="post-name"><a href="productdetails.html" class="linktopost">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                        <div class="post-meta">
                                            <a href="blogdetails.html" class="post-meta__item author"><img src="assets/images/home-03/post-author.png" width="28" height="28" alt=""><span>Admin</span></a>
                                        </div>
                                        <p class="excerpt">Did you know that red-staining foods are excellent lymph-movers? In fact, many plants that were historically used as dyes...</p>
                                        <div class="group-buttons">
                                            <a href="blogdetails.html" class="btn readmore">continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-item effect-01 style-bottom-info layout-02 ">
                                    <div class="thumbnail">
                                        <a href="blogdetails.html" class="link-to-post"><img src="assets/images/our-blog/post-thumb-01.jpg" width="370" height="270" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <h4 class="post-name"><a href="productdetails.html" class="linktopost">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                        <div class="post-meta">
                                            <a href="blogdetails.html" class="post-meta__item author"><img src="assets/images/home-03/post-author.png" width="28" height="28" alt=""><span>Admin</span></a>
                                        </div>
                                        <p class="excerpt">Did you know that red-staining foods are excellent lymph-movers? In fact, many plants that were historically used as dyes...</p>
                                        <div class="group-buttons">
                                            <a href="blogdetails.html" class="btn readmore">continue reading</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

        <!--Mobile Global Menu-->
        <div class="mobile-block-global">
            <div class="biolife-mobile-panels">
                <span class="biolife-current-panel-title">Global</span>
                <a class="biolife-close-btn" data-object="global-panel-opened" href="#">&times;</a>
            </div>
            <div class="block-global-contain">
                <div class="glb-item my-account">
                    <b class="title">My Account</b>
                    <ul class="list">
                        <li class="list-item"><a href="#">Login/register</a></li>
                        <li class="list-item"><a href="#">Cart <span class="index">(8)</span></a></li>
                        <li class="list-item"><a href="#">Checkout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!--Quickview Popup-->
        <div id="biolife-quickview-block" class="biolife-quickview-block">
            <div class="quickview-container">
                <a href="#" class="btn-close-quickview" data-object="open-quickview-block"><span class="biolife-icon icon-close-menu"></span></a>
                <div class="biolife-quickview-inner">
                    <div class="media">
                        <ul class="biolife-carousel quickview-for" data-slick='{"arrows":false,"dots":false,"slidesMargin":30,"slidesToShow":1,"slidesToScroll":1,"fade":true,"asNavFor":".quickview-nav"}'>
                            <li><img src="assets/images/details-product/detail_01.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_02.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_03.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_04.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_05.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_06.jpg" alt="" width="500" height="500"></li>
                            <li><img src="assets/images/details-product/detail_07.jpg" alt="" width="500" height="500"></li>
                        </ul>
                        <ul class="biolife-carousel quickview-nav" data-slick='{"arrows":true,"dots":false,"centerMode":false,"focusOnSelect":true,"slidesMargin":10,"slidesToShow":3,"slidesToScroll":1,"asNavFor":".quickview-for"}'>
                            <li><img src="assets/images/details-product/thumb_01.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_02.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_03.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_04.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_05.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_06.jpg" alt="" width="88" height="88"></li>
                            <li><img src="assets/images/details-product/thumb_07.jpg" alt="" width="88" height="88"></li>
                        </ul>
                    </div>
                    <div class="product-attribute">
                        <h4 class="title"><a href="#" class="pr-name">National Fresh Fruit</a></h4>
                        <div class="rating">
                            <p class="star-rating"><span class="width-80percent"></span></p>
                        </div>

                        <div class="price price-contain">
                            <ins><span class="price-amount"><span class="currencySymbol">£</span>85.00</span></ins>
                            <del><span class="price-amount"><span class="currencySymbol">£</span>95.00</span></del>
                        </div>
                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel maximus lacus. Duis ut mauris eget justo dictum tempus sed vel tellus.</p>
                        <div class="from-cart">
                            <div class="qty-input">
                                <input type="text" name="qty12554" value="1" data-max_value="20" data-min_value="1" data-step="1">
                                <a href="#" class="qty-btn btn-up"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
                                <a href="#" class="qty-btn btn-down"><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                            </div>
                            <div class="buttons">
                                <a href="#" class="btn add-to-cart-btn btn-bold">add to cart</a>
                            </div>
                        </div>

                        <div class="product-meta">
                            <div class="product-atts">
                                <div class="product-atts-item">
                                    <b class="meta-title">Categories:</b>
                                    <ul class="meta-list">
                                        <li><a href="#" class="meta-link">Milk & Cream</a></li>
                                        <li><a href="#" class="meta-link">Fresh Meat</a></li>
                                        <li><a href="#" class="meta-link">Fresh Fruit</a></li>
                                    </ul>
                                </div>
                                <div class="product-atts-item">
                                    <b class="meta-title">Tags:</b>
                                    <ul class="meta-list">
                                        <li><a href="#" class="meta-link">food theme</a></li>
                                        <li><a href="#" class="meta-link">organic food</a></li>
                                        <li><a href="#" class="meta-link">organic theme</a></li>
                                    </ul>
                                </div>
                                <div class="product-atts-item">
                                    <b class="meta-title">Brand:</b>
                                    <ul class="meta-list">
                                        <li><a href="#" class="meta-link">Fresh Fruit</a></li>
                                    </ul>
                                </div>
                            </div>
                            <span class="sku">SKU: N/A</span>
                            <div class="biolife-social inline add-title">
                                <span class="fr-title">Share:</span>
                                <ul class="socials">
                                    <li><a href="#" title="twitter" class="socail-btn"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#" title="facebook" class="socail-btn"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#" title="pinterest" class="socail-btn"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    <li><a href="#" title="youtube" class="socail-btn"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                    <li><a href="#" title="instagram" class="socail-btn"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="./layout/footer.jsp"/>

        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
    </body>

</html>