package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\r\n");
      out.write("<html class=\"no-js\" lang=\"en\">\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("    <title>Biolife - Organic Food</title>\r\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap\" rel=\"stylesheet\">\r\n");
      out.write("    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"assets/images/favicon.png\" />\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/animate.min.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/font-awesome.min.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/nice-select.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/slick.min.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/style.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"assets/css/main-color.css\">\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body class=\"biolife-body\">\r\n");
      out.write("               ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "./layout/header.jsp", out, false);
      out.write("\r\n");
      out.write("    <!--Navigation section-->\r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <nav class=\"biolife-nav\">\r\n");
      out.write("            <ul>\r\n");
      out.write("                <li class=\"nav-item\"><a href=\"home.html\" class=\"permal-link\">Home</a></li>\r\n");
      out.write("                <li class=\"nav-item\"><span class=\"current-page\">Register</span></li>\r\n");
      out.write("            </ul>\r\n");
      out.write("        </nav>\r\n");
      out.write("    </div>\r\n");
      out.write("\r\n");
      out.write("    <div class=\"page-contain login-page\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Main content -->\r\n");
      out.write("        <div id=\"main-content\" class=\"main-content\">\r\n");
      out.write("            <div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("                <div class=\"row\">\r\n");
      out.write("\r\n");
      out.write("                    <!--Form Sign In-->\r\n");
      out.write("                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n");
      out.write("                        <div class=\"signin-container\">\r\n");
      out.write("                            <form action=\"#\" name=\"frm-login\" method=\"post\">\r\n");
      out.write("                                <p class=\"form-row\">\r\n");
      out.write("                                    <label for=\"fid-name\">Username<span class=\"requite\">*</span></label>\r\n");
      out.write("                                    <input type=\"text\" id=\"fid-name\" name=\"name\" value=\"\" class=\"txt-input\">\r\n");
      out.write("                                </p>\r\n");
      out.write("                                <p class=\"form-row\">\r\n");
      out.write("                                    <label for=\"fid-name\">Email<span class=\"requite\">*</span></label>\r\n");
      out.write("                                    <input type=\"text\" id=\"fid-name\" name=\"name\" value=\"\" class=\"txt-input\">\r\n");
      out.write("                                </p>\r\n");
      out.write("                                <p class=\"form-row\">\r\n");
      out.write("                                    <label for=\"fid-pass\">Password:<span class=\"requite\">*</span></label>\r\n");
      out.write("                                    <input type=\"email\" id=\"fid-pass\" name=\"email\" value=\"\" class=\"txt-input\">\r\n");
      out.write("                                </p>\r\n");
      out.write("                                <p class=\"form-row\">\r\n");
      out.write("                                    <label for=\"fid-pass\">Re-Password:<span class=\"requite\">*</span></label>\r\n");
      out.write("                                    <input type=\"email\" id=\"fid-pass\" name=\"email\" value=\"\" class=\"txt-input\">\r\n");
      out.write("                                </p>\r\n");
      out.write("                                <p class=\"form-row wrap-btn\">\r\n");
      out.write("                                    <button class=\"btn btn-submit btn-bold\" type=\"submit\">Register</button>\r\n");
      out.write("                                </p>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <!--Go to Register form-->\r\n");
      out.write("                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n");
      out.write("                        <div class=\"register-in-container\">\r\n");
      out.write("                            <div class=\"intro\">\r\n");
      out.write("                                <h4 class=\"box-title\">Do you have an account?</h4>\r\n");
      out.write("\r\n");
      out.write("                                <a href=\"login.html\" class=\"btn btn-bold\">Login</a>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("\r\n");
      out.write("     ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "./layout/footer.jsp", out, false);
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <!-- Scroll Top Button -->\r\n");
      out.write("    <a class=\"btn-scroll-top\"><i class=\"biolife-icon icon-left-arrow\"></i></a>\r\n");
      out.write("\r\n");
      out.write("    <script src=\"assets/js/jquery-3.4.1.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/bootstrap.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/jquery.countdown.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/jquery.nice-select.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/jquery.nicescroll.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/slick.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/biolife.framework.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/functions.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
