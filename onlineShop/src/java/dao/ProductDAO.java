/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Product;
import model.User;

/**
 *
 * @author Admin
 */
public class ProductDAO extends DBContext {

    //when get product always get with condition and then if have sort we will sort with a method 
    public ArrayList<Product> getAllProduct() {
        ArrayList<Product> proList = new ArrayList<>();
        try {
            String sql = "select a.*,b.category from (select p.* , s.[value] as brand from product p , setting as s \n"
                    + "  where s.setting_id = p.brand_id) as a,\n"
                    + " (select p.product_id, s.[value] as category from product p , setting as s \n"
                    + "  where s.setting_id = p.category_id) as b\n"
                    + "  where a.product_id = b.product_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Product p = new Product(result.getInt("product_id"), result.getString("title"), result.getString("brand"), result.getDouble("price"), result.getDouble("sale_price"), result.getInt("stock"),
                        result.getString("description"), result.getString("img"), result.getString("category"), result.getDate("createDate"), new User(result.getInt("author_id")), result.getBoolean("active"));
                proList.add(p);
            }
        } catch (Exception e) {
        }
        return proList;
    }

    public ArrayList<Product> getAllProductByCategory() {
        ArrayList<Product> proList = new ArrayList<>();
        try {
            String sql = "";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                proList.add(p);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<Product> searchProduct() {
        ArrayList<Product> proList = new ArrayList<>();
        try {
            String sql = "";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                proList.add(p);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
