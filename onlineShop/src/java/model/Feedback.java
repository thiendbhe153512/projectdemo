/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author SHD
 */
public class Feedback {
    private int feedback_id;
    private int product_id;
    private int user_id;
    private int rated;
    private String fb_content;
    private String image;
    private Date feedback_date;
    private boolean active;

    public Feedback() {
    }

    public Feedback(int feedback_id, int product_id, int user_id, int rated, String fb_content, String image, Date feedback_date, boolean active) {
        this.feedback_id = feedback_id;
        this.product_id = product_id;
        this.user_id = user_id;
        this.rated = rated;
        this.fb_content = fb_content;
        this.image = image;
        this.feedback_date = feedback_date;
        this.active = active;
    }

    public int getFeedback_id() {
        return feedback_id;
    }

    public void setFeedback_id(int feedback_id) {
        this.feedback_id = feedback_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRated() {
        return rated;
    }

    public void setRated(int rated) {
        this.rated = rated;
    }

    public String getFb_content() {
        return fb_content;
    }

    public void setFb_content(String fb_content) {
        this.fb_content = fb_content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getFeedback_date() {
        return feedback_date;
    }

    public void setFeedback_date(Date feedback_date) {
        this.feedback_date = feedback_date;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
}
