/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author SHD
 */
public class CartDetail {
    private int cart_detail_id;
    private int cart_id;
    private int product_id;
    private int quantity;

    public CartDetail() {
    }

    public CartDetail(int cart_detail_id, int cart_id, int product_id, int quantity) {
        this.cart_detail_id = cart_detail_id;
        this.cart_id = cart_id;
        this.product_id = product_id;
        this.quantity = quantity;
    }

    public int getCart_detail_id() {
        return cart_detail_id;
    }

    public void setCart_detail_id(int cart_detail_id) {
        this.cart_detail_id = cart_detail_id;
    }

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    

}
