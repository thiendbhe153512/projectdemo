/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author SHD
 */
public class Cart {
    private int cart_id;
    private int user_id;
    private float total_price;

    public Cart() {
    }

    public Cart(int cart_id, int user_id, float total_price) {
        this.cart_id = cart_id;
        this.user_id = user_id;
        this.total_price = total_price;
    }

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }
    
}
